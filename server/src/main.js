'use strict'

import fs from 'fs'
import { randomBytes } from 'crypto'
import WebSocket from 'ws'
import EthConnector from './EthConnector.js'
import path from 'path'


const config = ((file) => {
    try {
        const res = JSON.parse(fs.readFileSync(file))
        const abiPath = path.join(
            path.dirname(new URL(import.meta.url).pathname),
            '../abi/IdentityManager.json'
        )

        res.ethereum.abi = JSON.parse(fs.readFileSync(abiPath)).abi

        return res
    } catch {
        console.error('Could not parse configuration file')
        process.exit(1)
    }
})(process.argv[2])

const server = new WebSocket.Server(config.server)
const ethConnector = new EthConnector(config.ethereum)

const channels = ['mail', 'snailmail', 'phonemessage'] // this is just an example
const checkFrame = f => ['address', 'channel', 'signature'].every(v => f[v])

process.on('SIGTERM', () => {
    server.close()
    ethConnector.close()
})


server.on('connection', client => {
    client.on('message', async message => {
        const input = (() => {
            try   { return JSON.parse(message) } 
            catch { return undefined }
        })()

        if (!input || !checkFrame(input) || !channels.includes(input.channel)) {
            client.send('NACK')
            client.close()
            return
        }

        const token = randomBytes(16).toString('hex')

        try {
            console.log(await ethConnector.approve(token, input.signature))
            client.send('OK')
            console.log(`Request ${input.address}, ${input.channel} resolved into token:\n${token}`)
        } catch (error) {
            client.send('ERR')
            console.error(error)
            console.log(error.message)
        } finally {
            client.close()
        }
    })
})
