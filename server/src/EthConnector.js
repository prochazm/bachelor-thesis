'use strict'

import Web3 from 'web3'
import { Transaction } from 'ethereumjs-tx'

export default class {
    constructor(config) {
        this.web3 = new Web3(config.url)
        this.account = this.web3.eth.accounts.privateKeyToAccount(config.privkey)
        this.identityManager = new this.web3.eth.Contract(config.abi, config.address)
    }

    async approve(token, signature) {
        const tokenHash = Web3.utils.soliditySha3Raw(`0x${token}`)
        const transaction = new Transaction({
            nonce:      await this.web3.eth.getTransactionCount(this.account.address, 'pending'),
            from:       this.account.address,
            to:         this.identityManager.options.address,
            gasPrice:   Web3.utils.toHex(await this.web3.eth.getGasPrice()),
            gasLimit:   Web3.utils.toHex(6721975),
            data:       this.identityManager.methods.approveIdentity(tokenHash, signature).encodeABI()
        })

        transaction.sign(Web3.utils.hexToBytes(this.account.privateKey))

        return (await this.web3.eth.sendSignedTransaction(transaction.serialize())).transactionHash
    }

    close() {
        this.web3.currentProvider.disconnect()
    }
}
