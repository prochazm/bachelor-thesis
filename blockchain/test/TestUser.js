const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
const assert = chai.assert

const User = artifacts.require('User')

chai.use(chaiAsPromised)



contract('User', function(accounts){

    let user

    beforeEach(async function() {
        user = await User.new(accounts[0], '0x01')
    })

    it('claimKeyBundle() should succeed', async function() {
        let user2 = await User.new(accounts[1], '0x01')
        let tx1 = user2.refreshSpk('0x02', '0x03', { from: accounts[1] } )
        let tx2 = user2.refreshOpkStore('0x04', 5, { from: accounts[1] } )
        let tx3 = user.claimKeyBundle(user2.address)
        let ofs = await user2.opkStoreOffset()
        let tsu = await user.lastKeyClaimTimestamp()
        let tsb = (await web3.eth.getBlock('latest')).timestamp


        tx3.then(r => {
            assert.equal(r.logs[0].event, 'keyBundleClaimed', 'Invalid event has been triggered')
            assert.equal(r.logs[0].args['owner'], accounts[1])
            assert.equal(r.logs[0].args['signedPrekey'], '0x02')
            assert.equal(r.logs[0].args['idSigned'], '0x03')
            assert.equal(r.logs[0].args['opkStoreAddress'], '0x04')
            assert.equal(r.logs[0].args['opkStoreOffset'], 0)
        })

        await assert.isFulfilled(tx1, 'Could not refresh SPK')
        await assert.isFulfilled(tx2, 'Could not refresh OPK')
        await assert.isFulfilled(tx3, 'Transaction should have been successful')

        assert.equal(ofs, 1)
        assert.equal(tsu.toNumber(), tsb)
    })

    it('claimKeyBundle() should revert with keys untitialized', async function() {
        let user2 = await User.new(accounts[1], '0x01')
        let tx = user.claimKeyBundle(user2.address)

        await assert.isRejected(tx, 'uninitialized', 'Transaction should have been reverted');
    })

    it('refreshSpk() should revert unless owned', async function() {
        let tx = user.refreshSpk('0x02', '0x03', { from: accounts[1] } )

        await assert.isRejected(tx, 'owner', 'Transaction should have been rejected')
    })

    it('refreshSpk() should succeed', async function() {
        let tx = user.refreshSpk('0x02', '0x03')
        let spk = await user.signedPrekey()
        let ids = await user.idSigned()

        await assert.isFulfilled(tx, 'Transaction should have been completed')
        assert.equal(spk, '0x02', 'SPK was not set properly')
        assert.equal(ids, '0x03', 'Identity signature was not set properly')
    })

    it('refreshOpkStore() should revert unless owned', async function() {
        let tx = user.refreshOpkStore('0x02', 3, { from: accounts[1]} )

        await assert.isRejected(tx, 'owner', 'Transaction should have been rejected')
    })

    it('refreshOpkStore() should succeed', async function() {
        let tx = user.refreshOpkStore('0x02', 3)
        let osa = await user.opkStoreAddress()
        let oss = await user.opkStoreSize()
        let ofs = await user.opkStoreOffset()

        await assert.isFulfilled(tx, 'Transaction should have been completed')
        assert.equal(osa, '0x02', 'Store address was not set properly')
        assert.equal(oss, 3, 'Store size was not set properly')
        assert.equal(ofs, 0, 'Store offset should have been zero')
    })
})
