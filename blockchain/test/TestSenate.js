const truffleAssert = require('truffle-assertions');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = chai.assert;

const Senate = artifacts.require("Senate");

chai.use(chaiAsPromised);

async function printVote(instance) {
    let struct = await instance.ballot();
    console.log("subject: \t" + struct[0]);
    console.log("deadline:\t" + struct[1].toNumber());
    console.log("votedFor:\t" + struct[2].toNumber());
    console.log("votedAgainst:\t" + struct[3].toNumber());
    console.log("id:\t\t" + struct[4].toNumber());
    console.log("description:\t" + struct[5]);
    console.log("topic:\t\t" + struct[6].toNumber());
    console.log("state:\t\t" + struct[7].toNumber() + "\n");
}


describe('Senate', function() {

    let instance;

    before(async function() {
        instance = await Senate.deployed();
    })

    contract('Happy day', function(accounts) {

        it('should initiate a new ballot', async function() {
            let promise = instance.createBallot(0, accounts[1], "xxx", {from: accounts[0]});
            let ballot  = await instance.ballot();
            let block   = await web3.eth.getBlock("latest");

            await chai.assert.isFulfilled(promise, "Transaction failed");
            assert.equal(ballot["subject"], accounts[1], "Ballot subject does not match");
            assert.equal(ballot["id"], 1, "Ballot id does not match");
            assert.equal(ballot["votedFor"], 0, "Ballot votesFor does not match");
            assert.equal(ballot["votedAgainst"], 0, "Ballot votesAgainst does not match");
            assert.equal(ballot["description"], "xxx", "Ballot description does not match");
            assert.equal(ballot["deadline"].toNumber(), block["timestamp"] + 60 * 60 * 24, "Ballot deadline does not match");
            assert.equal(ballot["topic"], 0, "Ballot topic does not match");
            assert.equal(ballot["state"], 1, "Ballot state does not match");
        });

        it('vote should be accepted', async function() {
            let promise = instance.vote(1, true);
            let ballot  = await instance.ballot();

            await assert.isFulfilled(promise, "Transaction failed");
            assert.equal(ballot["state"], 0, "Ballot state does not match");
            assert.equal(ballot["votedFor"], 1, "Ballot votedFor does not match");
        })
    });

    contract('Senate expansion', function(accounts) {
        before(async function() {
            for (let i = 1; i < 5; ++i) {
                await assert.isFulfilled(
                    instance.createBallot(0, accounts[i], "yyy", {from: accounts[0]}),
                    "failed to create new ballot");
                for (let j = 0; j < (i + 1) / 2; ++j) {
                    await assert.isFulfilled(
                        instance.vote(i, true, {from: accounts[j]}),
                        "failed to vote for ballot"
                    );
                }
            }
        });

        it('new senator should be able to initiate another ballot', async function() {
            let promise = instance.createBallot(0, accounts[5], "zzz", {from: accounts[4]});
            await chai.assert.isFulfilled(promise, "cannot be resolved", "did not reject");
        });

        it('new senator should be able to vote against the proposition', async function() {
            await assert.isFulfilled(instance.vote(5, false, {from: accounts[3]}), "vote failed");
            assert.equal((await instance.ballot())["votedAgainst"], 1, "Ballot votesAgainst does not match");
        });
    });


    contract('Breaking constraints', function(accounts) {
        before(async function() {
            instance.createBallot(0, accounts[1], "xxx", {from: accounts[0]});
        });

        it('non-senator should not be able to vote', async function() {
            await assert.isRejected(
                instance.vote(1, true, {from: accounts[1]}),
                "senators only",
                "non-senator was able to vote"
            );
        })

        it('non-senator should not be able to create ballots', async function() {
            await assert.isRejected(
                instance.createBallot(0, accounts[1], "xxx", {from: accounts[1]}),
                "senators only",
                "non-senator was able to create a ballot"
            );
        });

        it('voting with invalid ballot id should revert transaction', async function() {
            await assert.isRejected(instance.vote(723, true), "ballot id", "vote was not rejected");
        });

        it('senator should be able to only vote once on each ballot', async function() {
            instance.vote(1, true);
            instance.createBallot(0, accounts[2], "yyy");
            instance.vote(2, true);

            await assert.isRejected(instance.vote(2, false), "only once", "second vote was not rejected");
        })
    })

    contract('Breaking constraints 2', async function(accounts) {

        before(async function() {
            instance.createBallot(2, accounts[8], "invite authority")
            instance.vote(1, true)
            instance.createBallot(0, accounts[7], "invite senator")
            instance.vote(2, true)
        })

        it('voting for invitation of active authority account should fail', async function() {
            await assert.isRejected(
                instance.createBallot(0, accounts[7], "invite existing senator", { from: accounts[0] }),
                "already present",
                "existing senator cannot be invited"
            )
        })

        it('voting for revocation of non-senator account should fail', async function() {
            await assert.isRejected(
                instance.createBallot(1, accounts[9], "kick non-senator", { from: accounts[0] }),
                "does not exist",
                "non-senator cannot be kicked"
            )
        })

        it('voting for invitation of active authority account should fail', async function() {
            await assert.isRejected(
                instance.createBallot(2, accounts[8], "invite active authority", { from: accounts[0] }),
                "already active",
                "active authority cannot be invited"
            )
        })

        it('voting for revocation of non-authority account should fail', async function() {
            await assert.isRejected(
                instance.createBallot(3, accounts[9], "kick non-authority", { from: accounts[0] }),
                "not active",
                "inactive authority cannot be kicked"
            )
        })
    })
})
