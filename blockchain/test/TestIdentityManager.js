const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = chai.assert;

const IdentityManager = artifacts.require("IdentityManager");
const UserManager = artifacts.require('UserManager');
const User = artifacts.require('User');

chai.use(chaiAsPromised);



contract('IdentityManager', function(accounts){

    let userManager;
    let identityManager;
    let user;

    beforeEach(async function() {
        userManager = await UserManager.new();
        identityManager = await IdentityManager.new(userManager.address);
        await identityManager.approveAuthority(accounts[1]);
        await userManager
            .signUp('0x000000ea', {from: accounts[2]})
            .then(()  => { return userManager.users(accounts[2]) })
            .then(adr => { return User.at(adr) })
            .then(usr => { 
                user = usr;
                return user.refreshSpk('0x01', '0x02', {from: accounts[2]}) 
            })
            .then(()  => { return user.refreshOpkStore('0x03', 4, {from: accounts[2]}) });

    });

    it('Full protocol run should succeed', async function(){
        const address = "x@y.xyz";
        const token = web3.utils.asciiToHex("xxx");
        const token_hash = web3.utils.keccak256(token);
        const address_hash = web3.utils.keccak256(web3.utils.utf8ToHex(address));
        const signature = await web3.eth.sign(address, accounts[2]);

        await assert.isFulfilled(identityManager.approveIdentity(token_hash, signature, { from: accounts[1] }));
        await assert.isFulfilled(identityManager.claimIdentity(token, address_hash, { from: accounts[2] }));
    });

    it('approveIdentity() should trigger event', async function() {
        const token_hash = web3.utils.keccak256(web3.utils.asciiToHex('test'));

        await identityManager.approveIdentity(token_hash, '0x00', { from: accounts[1] }).then(r => {
            assert.equal(r.logs[0].event, 'IdentityApproved');
            assert.equal(r.logs[0].args['tokenHash'], token_hash, "Event triggered with invalid tokenHash");
        });
    })

    it('approveIdentity() should revert unless passing unique token', async function() {
        let tx1 = identityManager.approveIdentity('0x01', '0x00', { from: accounts[1] });
        let tx2 = identityManager.approveIdentity('0x01', '0x00', { from: accounts[1] });

        await assert.isFulfilled(tx1, 'Initial transaction should have been fulfilled');
        await assert.isRejected(tx2, 'not unique', 'Second transaction should have been rejected')
    });

    it('approveIdentity() should revert unless caller is authority', async function() {
        let tx = identityManager.approveIdentity('0x01', '0x00', { from: accounts[0] });

        await assert.isRejected(tx, 'no authority', 'Transaction should have been rejected');
    })

    it('claimIdentity() should revert unless caller is user', async function() {
        let tx = identityManager.claimIdentity('0x01', '0x01', { from: accounts[3] });

        await assert.isRejected(tx, 'not a user', 'Transaction should have been rejected');
    })

    it('claimIdentity() should emit failure', async function() {
        await identityManager.approveIdentity(web3.utils.keccak256('test2'), '0x01', { from: accounts[1] })
        await identityManager.claimIdentity(web3.utils.utf8ToHex('test2'), '0x01', { from: accounts[2] }).then(r => {
            assert.equal(r.logs[0].event, 'IdentityClaimed', 'Invalid event has been emited');
            assert.equal(r.logs[0].args['tokenHash'], web3.utils.keccak256('test2'), 'IdentityClaimed.tokenHash does not match');
            assert.equal(r.logs[0].args['success'], false, 'IdentityClaimed.success should have been false');
        });
    })

    it('claimIdentity() should emit success', async function() {
        const addressHash = web3.utils.keccak256('test');
        const signature = await web3.eth.sign(addressHash, accounts[2]);
        const tokenHash = web3.utils.keccak256('0x01');


        let tx1 = identityManager.approveIdentity(tokenHash, signature, { from: accounts[1] });
        
        await assert.isFulfilled(tx1, 'Identity could not be approved');
        await identityManager.claimIdentity('0x01', addressHash, { from: accounts[2] }).then(r => {
            assert.equal(r.logs[0].event, 'IdentityClaimed', 'Invalid event has been emited');
            assert.equal(r.logs[0].args['tokenHash'], tokenHash, 'IdentityClaimed.tokenHash does not match');
            assert.equal(r.logs[0].args['success'], true, 'IdentityClaimed.success should have been true');
        });
    })

    it('rejectIdentity() should remove identity', async function() {
        const tokenHash = web3.utils.keccak256('0x01');

        let tx1 = identityManager.approveIdentity(tokenHash, '0x02', { from: accounts[1] })
        let tx2 = identityManager.rejectIdentity('0x01');
        let idq = await identityManager.identityQueue(tokenHash);

        await assert.isFulfilled(tx1, 'Failed to approve identity');
        await assert.isFulfilled(tx2, 'Failed to reject identity');
        assert.isNull(idq['0'], 'Signature was not removed from the queue');
        assert.equal(parseInt(idq['1']), 0, 'Authority was not removed from the queue');
    })

    it('verify() should return success', async function() {
        let h = web3.utils.keccak256('test');
        let sig = await web3.eth.sign(h, accounts[0]);
        let tx = await identityManager.verify(h, accounts[0], sig);

        assert.isTrue(tx, "Signature should have been matching");
    })

    it('verify() should return failure on invalid signer', async function() {
        let h = web3.utils.keccak256('test');
        let sig = await web3.eth.sign(h, accounts[1]);
        let tx = await identityManager.verify(h, accounts[0], sig);

        assert.isFalse(tx, "Signer does not match, return value should have been false")
    })

    it('verify() should return failure on corrupted hash', async function() {
        let h = web3.utils.keccak256('test');
        let sig = await web3.eth.sign(h, accounts[1]);
        let tx = await identityManager.verify(web3.utils.keccak256(h), accounts[0], sig);

        assert.isFalse(tx, "Hash does not match, return value should have been false")
    })

    it('approveAuthority() should revert unless owned', async function() {
        let tx = identityManager.approveAuthority(accounts[9], { from: accounts[1] });

        await assert.isRejected(tx, 'owner', 'Transaction should have been rejected');
    })

    it('approveAuthority() should succeed', async function() {
        let tx = identityManager.approveAuthority(accounts[9], { from: accounts[0] });
        let authority = await identityManager.authorities(accounts[9]);

        await assert.isFulfilled(tx, 'Transaction should have been rejected');
        assert.isTrue(authority.active, 'Authority was approved but active flag is still false');
    })

    it('setAuthorityDescription() should revert unless caller is authority', async function() {
        let tx = identityManager.setAuthorityDescription('test');

        await assert.isRejected(tx, 'authority', 'Transaction should have been rejected');
    })

    it('setAuthorityDescription() should succeed', async function() {
        let tx = identityManager.setAuthorityDescription('test', { from: accounts[1] });
        let authority = await identityManager.authorities(accounts[1]);

        await assert.isFulfilled(tx, 'Transaction should have been rejected');
        assert.equal(authority.description, 'test', 'Description does not match');
    })
});
