// SPDX-License-Identifier: GPL v3
pragma solidity ^0.8.1;

import "./OwnableBisem.sol";


contract User is OwnableBisem {
    bytes   public identityKey;
    bytes   public signedPrekey;
    bytes   public idSigned;
    bytes   public opkStoreAddress;
    uint16  public opkStoreOffset;
    uint16  public opkStoreSize;
    uint256 public lastKeyClaimTimestamp;

    event keyBundleClaimed(
        address indexed owner,
        bytes   signedPrekey, 
        bytes   idSigned, 
        bytes   opkStoreAddress, 
        uint16  opkStoreOffset
    );

    constructor(address _owner, bytes memory _identityKey) 
    OwnableBisem(_owner) {
        owner = _owner;
        identityKey = _identityKey;
    }

    modifier hasKeys(User _u) {
        require(_u.signedPrekey().length != 0, "SPK uninitialized");
        require(_u.idSigned().length != 0, "sig(IK) uninitialized");
        require(_u.opkStoreAddress().length != 0, "OPK store unitialized");
        _;
    }

    function claimKeyBundle(User _target)
    external owned hasKeys(_target) {
        bisemSignal(address(_target));

        emit keyBundleClaimed(
            _target.owner(), 
            _target.signedPrekey(), 
            _target.idSigned(), 
            _target.opkStoreAddress(), 
            _target.opkNext()
        );
    }

    function refreshSpk(bytes calldata _signedPrekey, bytes calldata _idSigned)
    external owned {
        signedPrekey    = _signedPrekey;
        idSigned        = _idSigned;
    }

    function refreshOpkStore(bytes calldata _storeAddress, uint16 _storeSize)
    external owned {
        opkStoreAddress = _storeAddress;
        opkStoreSize    = _storeSize;
        opkStoreOffset  = 0;
    }

    function updateTimestamp() 
    public bisemProtected {
        lastKeyClaimTimestamp = block.timestamp;
    }

    function opkNext()
    public returns (uint16) {
        require(opkStoreOffset < opkStoreSize, "OPK store exhausted");
        require(block.timestamp - User(msg.sender).lastKeyClaimTimestamp() > 10 minutes, "Wait 10 minutes to claim nother key bundle");

        User(msg.sender).updateTimestamp();

        return opkStoreOffset++;
    }
}
