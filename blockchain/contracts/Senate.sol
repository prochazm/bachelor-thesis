// SPDX-License-Identifier: GPL v3
pragma solidity ^0.8.1;

import "./LAddressSet.sol";
import "./IdentityManager.sol";


contract Senate {
    enum Topic { S_INVITE, S_REMOVE, VA_INVITE, VA_REMOVE }
    enum State { RESOLVED, PENDING }

    struct Ballot {
        address subject;
        uint256 deadline;
        uint16 votedFor;
        uint16 votedAgainst;
        uint16 id;
        string description;
        Topic topic;
        State state;
    }

    event ballotResolution(
        address indexed subject,
        uint16  indexed id,
        uint16          votedFor,
        uint16          votedAgainst,
        string          description,
        Topic   indexed topic,
        bool            verdict
    );

    using LAddressSet for LAddressSet.container;

    IdentityManager idManager;
    LAddressSet.container senators;
    Ballot public ballot;

    constructor(address _idManagerAddress) {
        idManager = IdentityManager(_idManagerAddress);
        senators.insert(msg.sender);
    }

    modifier onlySenators() {
        require(senators.contains(msg.sender), "Restricted to senators only");
        _;
    }

    function createBallot(Topic _topic, address _subject, string calldata _description)
    external onlySenators {
        if (_topic == Topic.S_INVITE) {
            require(!senators.contains(_subject), "Senator is already present");
        } else if (_topic == Topic.S_REMOVE) {
            require(senators.contains(_subject), "Senator does not exist");
        } else if (_topic == Topic.VA_INVITE) {
            (, bool isValidAuthority) = idManager.authorities(_subject);
            require(!isValidAuthority, "Authority is already active");
        } else if (_topic == Topic.VA_REMOVE) {
            (, bool isValidAuthority) = idManager.authorities(_subject);
            require(isValidAuthority, "Authority is not active");
        }

        if (isVotable()) {
            if (isExpired()) {
                votedMajorityClose();
            } else {
                require(isClosable(), "Current ballot cannot be resolved");
                absoluteMajorityClose();
            }
        }

        ballot = Ballot({
            topic: _topic, 
            subject: _subject, 
            deadline: block.timestamp + 1 days,
            description: _description,
            state: State.PENDING,
            votedFor: 0,
            votedAgainst: 0,
            id: ballot.id + 1
        });
   }

    function vote(uint16 _id, bool _verdict)
    external onlySenators {
        require(ballot.id == _id, "Invalid ballot id");
        require(!senators.hasVoted(msg.sender), "You can vote only once");
        require(isVotable(), "Ballot is not votable");

        if (isExpired()) {
            votedMajorityClose();
        } else {
            _verdict 
            ? ballot.votedFor++
            : ballot.votedAgainst++;
            senators.setVoted(msg.sender);

            if (isClosable()) {
                absoluteMajorityClose();
            }
        }
    }

    function update() 
    external {
        require(isExpired(), "Ballot cannot be resolved");
        votedMajorityClose();
    }

    function resolve(bool _accepted)
    private {
        if (_accepted) {
            if (ballot.topic == Topic.S_INVITE) {
                senators.insert(ballot.subject);
            } else if (ballot.topic == Topic.S_REMOVE) {
                senators.remove(ballot.subject);
            } else if (ballot.topic == Topic.VA_INVITE) {
                idManager.approveAuthority(ballot.subject);
            } else if (ballot.topic == Topic.VA_REMOVE) {
                idManager.revokeAuthority(ballot.subject);
            }
        }

        ballot.state = State.RESOLVED;
        senators.resetVoting();
        emit ballotResolution(
            ballot.subject,
            ballot.id,
            ballot.votedFor,
            ballot.votedAgainst,
            ballot.description,
            ballot.topic,
            _accepted
        );
    }

    function absoluteMajorityClose() 
    private {
        resolve(ballot.votedFor > acceptanceLimit());
    }

    function votedMajorityClose() 
    private {
        resolve(ballot.votedFor > ballot.votedAgainst);
    }

    function acceptanceLimit()
    private view returns (uint) {
        return senators.size() / 2;
    }

    function isExpired()
    private view returns (bool){
        return block.timestamp >= ballot.deadline;
    }

    function isClosable()
    private view returns (bool){
        return ballot.votedFor > acceptanceLimit()
            || ballot.votedAgainst > acceptanceLimit();
    }

    function isVotable()
    private view returns (bool){
        return ballot.state == State.PENDING;
    }
}
