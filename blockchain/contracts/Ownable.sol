// SPDX-License-Identifier: GPL v3
pragma solidity ^0.8.1;


contract Ownable {
    address public owner;

    constructor(address _owner) {
        owner = _owner;
    }

    modifier owned() {
        require(owner == msg.sender, "Caller has to be the owner");
        _;
    }

    modifier unowned() {
        require(owner != msg.sender, "Caller cannot be owner");
        _;
    }

    function changeOwner(address _owner)
    external owned {
        owner = _owner;
    }
}
