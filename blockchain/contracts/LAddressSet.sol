// SPDX-License-Identifier: GPL v3
pragma solidity ^0.8.1;


library LAddressSet {

    struct val {
        uint8 index;
        bool present;
        bool voted;
    }

    struct container {
        mapping(address => val) data;
        address[] keys;
    }

    function insert(container storage _self, address _value)
    public {
        assert(!_self.data[_value].present);
        _self.data[_value] = val({
            index: uint8(_self.keys.length),
            present: true,
            voted: false
        });
        _self.keys.push(_value);
    }

    function hasVoted(container storage _self, address _value)
    public view returns(bool) {
        assert(_self.data[_value].present);
        return _self.data[_value].voted;
    }

    function setVoted(container storage _self, address _value)
    public {
        assert(_self.data[_value].present);
        _self.data[_value].voted = true;
    }

    function resetVoting(container storage _self)
    public {
        for (uint i = 0; i < _self.keys.length; ++i) {
            _self.data[_self.keys[i]].voted = false;
        }
    }

    function remove(container storage _self, address _value)
    public {
        assert(_self.data[_value].present);
        _self.keys[_self.data[_value].index] = _self.keys[_self.keys.length - 1];
        _self.keys.pop();
        delete _self.data[_value];
    }

    function contains(container storage _self, address _value)
    public view returns (bool) {
        return _self.data[_value].present;
    }

    function size(container storage _self)
    public view returns (uint256) {
        return _self.keys.length;
    }
}
