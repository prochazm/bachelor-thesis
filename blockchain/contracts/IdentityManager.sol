// SPDX-License-Identifier: GPL v3
pragma solidity ^0.8.1;

import "./Ownable.sol";
import "./User.sol";
import "./UserManager.sol";


contract IdentityManager is Ownable {

    struct Identity {
        User user;
        uint256[] validationTimestamp;
        address[] validators;
    }

    struct Authority {
        string description;
        bool active;
    }

    struct IdentityTicket {
        bytes signature;
        address authority;
    }

    mapping(bytes32 => IdentityTicket) public identityQueue;    // H(T) => sig(Adr)
    mapping(bytes32 => Identity)              identityMapping;  // H(Adr) => User
    mapping(address => Authority)      public authorities;
    UserManager userManager;

    event IdentityApproved(bytes32 indexed tokenHash);
    event IdentityClaimed(bytes32 indexed tokenHash, bool success);

    constructor(UserManager _userManager)
    Ownable(msg.sender) {
        userManager = _userManager;
    }

    modifier onlyAuthority() {
        require(authorities[msg.sender].active, "Sender has no authority here");
        _;
    }

    function getIdentity(bytes32 _addrHash)
    external view returns (Identity memory ) {
        return identityMapping[_addrHash];
    }

    /// t2: server->contract call
    function approveIdentity(bytes32 _tokenHash, bytes calldata _sigAddr) 
    external onlyAuthority {
        require(identityQueue[_tokenHash].signature.length == 0, "Token is not unique");

        identityQueue[_tokenHash] = IdentityTicket({
            signature: _sigAddr,
            authority: msg.sender
        });

        emit IdentityApproved(_tokenHash);
    }

    /// t2: client->contract call
    function claimIdentity(bytes calldata _tokenPlain, bytes32 _addrHash)
    external {
        User user = userManager.users(msg.sender);

        require(address(user) != address(0), "Caller is not a user, did you sign up?");

        Identity storage id = identityMapping[_addrHash];
        bytes32 tokenHash = keccak256(_tokenPlain);

        require(identityQueue[tokenHash].signature.length != 0, "Ticket not found, listen for IdentityApproved");

        bool success = verify(_addrHash, msg.sender, identityQueue[tokenHash].signature);
        if (success){
            id.user = user;
            id.validationTimestamp.push(block.timestamp);
            id.validators.push(identityQueue[tokenHash].authority);
        } 

        delete identityQueue[tokenHash];

        emit IdentityClaimed(tokenHash, success);
    }

    function rejectIdentity(bytes calldata _tokenPlain) 
    external {
        delete(identityQueue[keccak256(_tokenPlain)]);
    }

    function deleteIdentity(bytes32 _addrHash)
    external {
        require(identityMapping[_addrHash].user.owner() == msg.sender);
        delete(identityMapping[_addrHash]);
    }

    function revokeIdentity(bytes32 _addrHash)
    external onlyAuthority {
        Identity storage id = identityMapping[_addrHash];
        uint length = id.validators.length;

        for (uint i = 0; i < length; ++i) {
            if (id.validators[i] == msg.sender) {
                id.validators[i] = id.validators[length - 1];
                id.validationTimestamp[i] = id.validationTimestamp[length - 1];
                delete id.validators[length - 1];
                delete id.validationTimestamp[length - 1];
                break;
            }
        }
    }

    function approveAuthority(address _address)
    external owned {
        authorities[_address].active = true;
    }

    function revokeAuthority(address _address)
    external owned {
        authorities[_address].active = false;
    }

    function setAuthorityDescription(string calldata description)
    external onlyAuthority {
        authorities[msg.sender].description = description;
    }

    function verify(bytes32 _h, address _signer, bytes memory _sig)
    public pure returns (bool) {
        bytes memory prefix = "\x19Ethereum Signed Message:\n32";
        bytes32 prefixedHash = keccak256(abi.encodePacked(prefix, _h));

        bytes32 r;
        bytes32 s;
        uint8   v;

        assembly {
            r := mload(add(_sig, 32))
            s := mload(add(_sig, 64))
            v := byte(0, mload(add(_sig, 96)))
        }

        if (v < 27) {
            v += 27;
        }

        return _signer == ecrecover(prefixedHash, v, r, s);
    }
}
