// SPDX-License-Identifier: GPL v3
pragma solidity ^0.8.1;

import "./Ownable.sol";


contract OwnableBisem is Ownable {
    address holder;


    constructor(address _owner) 
    Ownable(_owner) {}

    modifier bisemProtected() {
        require(holder == msg.sender);
        holder = address(0);
        _;
    }

    function bisemSignal(address _holder) 
    internal owned {
        holder = _holder;
    }
}
