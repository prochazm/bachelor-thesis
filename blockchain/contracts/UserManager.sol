// SPDX-License-Identifier: GPL v3
pragma solidity ^0.8.1;

import "./User.sol";
import "./Ownable.sol";


contract UserManager is Ownable {
    mapping(address => User) public users;

    constructor() Ownable(msg.sender) {}

    function signUp(bytes calldata _identityKey)
    external {
        require(
            address(users[msg.sender]) == address(0),
            "User already exists"
        );

        users[msg.sender] = new User(msg.sender, _identityKey);
    }
}
