var senate = artifacts.require("Senate");
var libAddressSet = artifacts.require("LAddressSet");
var userManager = artifacts.require("UserManager");
var identityManager = artifacts.require("IdentityManager");

module.exports = deployer => {
    deployer.deploy(userManager)
        .then(() => { return deployer.deploy(libAddressSet) })
        .then(() => { return deployer.link(libAddressSet, senate) })
        .then(() => { return deployer.deploy(identityManager, userManager.address) })
        .then(() => { return deployer.deploy(senate, identityManager.address) })
        .then(() => { return identityManager.deployed().then(i => i.changeOwner(senate.address)) });
};
