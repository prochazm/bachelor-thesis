#!/bin/sh

apt-get -y install curl
curl -fsSL https://deb.nodesource.com/setup_current.x | bash -
apt-get install -y nodejs
npm i -g truffle ganache-cli
