#!/bin/sh

CONF="$2"
BASE_PATH="$(dirname $0)"
CLIENT_PATH="${BASE_PATH}/client/src"
SERVER_PATH="${BASE_PATH}/server/src"
NODE_VERSION_REQ="v15.0.0"
LOWER_NODE_VERSION=$(echo "$(node -v)\n$NODE_VERSION_REQ" | sort | head -n1)

if [ "$NODE_VERSION_REQ" != "$LOWER_NODE_VERSION" ]; then
    echo "Update node to version ${NODE_VERSION_REQ} or later"
fi

if [ -z "$1" ]; then
    echo "First argument should be 'server' or 'client'"
    exit 1
fi

case $1 in
    "client") EXEC_DIR=$CLIENT_PATH ;;
    "server") EXEC_DIR=$SERVER_PATH ;;
    *)  echo "First argument should be 'server' or 'client'"
        exit 1 ;;
esac

if [ -z "$CONF" ]; then
    CONF="${EXEC_DIR}/default_config.json"
    echo "Using default configuration file ${CONF}"
fi

if [ ! -f "$CONF" ]; then
    echo "Configuration file ${CONF} does not exist"
    exit 2
fi

node "${EXEC_DIR}/main.js" "${CONF}" 2>/dev/null
