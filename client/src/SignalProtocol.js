'use strict'

import signal from 'libsignal'
import crypto from 'crypto'
import fs from 'fs'


function *opkGenerator(count) {
    for (let i = 0; i < count; ++i) {
        yield signal.keyhelper.generatePreKey(i)
    }
}

function toDeviceId(device) {
    const hash = crypto.createHash('sha256');
    const digest = hash.update(device).digest('hex').substr(0, 16)

    return parseInt(digest, 16)
}


class SignalProtocol {

    #remote
    #local
    #defaultKeyBatchSize

    constructor(remote, local, defaultKeyBatchSize = 10) {
        this.#remote = remote
        this.#local  = local
        this.#defaultKeyBatchSize = defaultKeyBatchSize
    }

    async signUp() {
        // prepare keys
        const identityKey    = signal.keyhelper.generateIdentityKeyPair()
        const registrationId = signal.keyhelper.generateRegistrationId()
        const opkArray       = [...opkGenerator(10)]
        const signedPreKey   = signal.keyhelper.generateSignedPreKey(identityKey, 0)

        // update remote storage (blocking)
        const registrationResult = await this.#remote.register({
            identityKey:    identityKey.pubKey,
            registrationId: registrationId,
            preKey: opkArray.map(key => 
                key.keyPair.pubKey
            ),
            signedPreKey: { 
                publicKey:  signedPreKey.keyPair.pubKey,
                signature:  signedPreKey.signature
            }
        })

        // update local storage only if the above does not throw
        this.#local.put('identityKey', identityKey)
        this.#local.put('registrationId', registrationId)
        this.#local.storeSignedPreKey(signedPreKey.keyId, signedPreKey.keyPair)
        opkArray.forEach((opk) => this.#local.storePreKey(opk.keyId, opk.keyPair))

        return registrationResult
    }

    async send(device, input, output) {
        const plaintext = fs.readFileSync(input)
        const data = await this.encrypt(device, plaintext)
        
        data.packet.body = data.packet.body.toString('base64')

        if (output) {
            fs.writeFileSync(output,JSON.stringify(data.packet))
        }

        return [ data.txHash, data.packet ].filter(i => i)
    }

    async encrypt(device, plaintext) {
        const userId = await this.#remote.getUserId(device)
        const deviceId = toDeviceId(device)
        const receiverAddress = new signal.ProtocolAddress(userId, deviceId)
        const result = { }

        await this.#local.loadSession(receiverAddress.toString()) ?? await (async () => {
            const [ txHash, keyBundle ] = await this.#remote.getPreKeyBundle(device)
            const sessionBuilder = new signal.SessionBuilder(this.#local, receiverAddress)
            
            keyBundle.registrationId = deviceId
            result.txHash = txHash

            await sessionBuilder.initOutgoing(keyBundle)
        })()

        const cipher = new signal.SessionCipher(this.#local, receiverAddress)
        result.packet = await cipher.encrypt(Buffer.from(plaintext))

        return result
    }

    async receive(device, input, output) {
        const ciphertext = JSON.parse(fs.readFileSync(input))
        ciphertext.body = Buffer.from(ciphertext.body, 'base64')
        const plaintext = await this.decrypt(ciphertext, device)
        if (output) {
            fs.writeFileSync(output, plaintext)
        }

        return plaintext
    }

    async decrypt(ciphertext, senderDevice) {
        const senderIdentity = await this.#remote.getUserId(senderDevice)
        const senderAddress = new signal.ProtocolAddress(senderIdentity, toDeviceId(senderDevice))
        const sessionCipher = new signal.SessionCipher(this.#local, senderAddress)

        const plaintext = await (ciphertext.type == 3
            ? sessionCipher.decryptPreKeyWhisperMessage
            : sessionCipher.decryptWhisperMessage
        ).call(sessionCipher, ciphertext.body)

        return plaintext.toString('utf8')
    }

    async updatePreKey(count = this.#defaultKeyBatchSize) {
        const opkArray = [...opkGenerator(count)]

        opkArray.forEach((opk) => this.#local.storePreKey(opk.keyId, opk.keyPair))

        return this.#remote.replaceOneTimePreKeys(opkArray.map(key => key.keyPair.pubKey))
    }

    async updateSignedPreKey() {
        const signedPreKey = signal.keyhelper.generateSignedPreKey(identityKey, 0)
        this.#local.storeSignedPreKey(signedPreKey.keyId, signedPreKey.keyPair)

        return this.#remote.replaceSignedPreKey(signedPreKey)
    }

    async clearSessions() {
        await this.#local.removeAllSessions('')
    }

    async getKeyStatus(user) {
        const values = await this.#remote.getPreKeyStatus(user)

        return [
            `Storage cid:\t${values[0]}`,
            `Used keys:\t${values[1]} out of ${values[2]}`,
            `Remaining keys:\t${values[2] - values[1]}`
        ]
    }

    async changeDevice(device) {
        this.#local.setDevice(toDeviceId(device))
    }
}

export default SignalProtocol
