'use strict'

import WebSocket from 'ws'


export const requestVerification = async (url, frame) => {
    const socket = new WebSocket(url)

    return new Promise((resolve, reject) => {
        socket.on('open', () => {
            // don't send garbage...
            if (!frame?.address)   reject('Address undefined')
            if (!frame?.channel)   reject('Channel undefined')
            if (!frame?.signature) reject('Signature undefined')

            socket.send(JSON.stringify(frame))
        })

        socket.on('message', (message) => {
            socket.close()
            switch (message) {
                case 'OK':   return resolve('')
                case 'ERR':  return reject('Validation request has failed (server-side)')
                default:     return reject('Validation response is corrupted')
            }
        })

        socket.on('error', (error) => {
            reject(`Failed to connect to authority ${url}, ${error.code}`)
        })
    })
}
