'use strict';

import Web3 from 'web3'
import fs from 'fs'
import { Transaction } from 'ethereumjs-tx'
import { fileURLToPath } from 'url'
import { dirname } from 'path'


function abiLoad(contract) {
    const directory = dirname(fileURLToPath(import.meta.url))
    const abi = JSON.parse(fs.readFileSync(`${directory}/abi/${contract}.json`))?.abi

    if (abi == null) {
        throw new Error(`Could not parse JSON abi of ${contract}`)
    }

    return abi
}

function toTransactionArgument(value) {
    if (typeof value === 'number' || Web3.utils.isHexStrict(value)) {
        return value
    }

    if (Web3.utils.isHex(value)) {
        return `0x${value}`
    }

    return Web3.utils.toHex(value)
}

function hash(...values) {
    return Web3.utils.soliditySha3Raw(...values.map(toTransactionArgument))
}

function hashString(...values) {
    return Web3.utils.soliditySha3Raw(...values)
}

class EthConnector {
    
    #userCache = []


    constructor(config) {
        if (config == null) {
            throw new Error('Ethereum configuration missing')
        }

        this.web3 = new Web3(`${config.node.protocol}://${config.node.host}:${config.node.port}`)

        const initContract = (name) => {
            if (config.contracts?.[name] == null) {
                throw new Error(`Contract ${n} address undefined`)
            }

            return new this.web3.eth.Contract(abiLoad(name), config.contracts[name])
        }

        this.userManager = initContract('UserManager')
        this.identityManager = initContract('IdentityManager')
        this.senate = initContract('Senate')

        this.account = ((key) => {
            if (!key) {
                throw new Error('Account undefined')
            }

            return this.web3.eth.accounts.privateKeyToAccount(key)
        })(config.privkey)
    }

    async sendTransaction(contract, method, args = [], value = 0) {
        const methodCall = contract.methods[method](...args.map(toTransactionArgument))

        const transaction = new Transaction({
            nonce:      await this.web3.eth.getTransactionCount(this.account.address, 'pending'),
            from:       this.account.address,
            to:         contract.options.address,
            gasPrice:   Web3.utils.toHex(await this.web3.eth.getGasPrice()),
            gasLimit:   Web3.utils.toHex(6721975), // TODO: make this number less magic
            value:      Web3.utils.toHex(value),
            data:       methodCall.encodeABI()
        })

        transaction.sign(Web3.utils.hexToBytes(this.account.privateKey))

        const receipt = await this.web3.eth.sendSignedTransaction(transaction.serialize())

        return receipt.transactionHash
    }

    async userInstantiate(cacheKey, address) {
        return this.#userCache[cacheKey] ??= new this.web3.eth.Contract(abiLoad('User'), address)
    }
    
    async getUserContract(account = this.account.address) {
        const address = await this.userManager.methods.users(account).call()

        if (0 === parseInt(address, 16)) {
            throw 'User not found'
        }

        return this.userInstantiate(account, address)
    }
    
    async getUserContractById(identity) {
        const identityHash = hashString(identity)
        const address = (await this.identityManager.methods.getIdentity(identityHash).call()).user

        if (0 === parseInt(address, 16)) {
            throw 'User not found'
        }

        return this.userInstantiate(identityHash, address)
    }

    async getIdentityKey(identity) {
        const contract = await this.getUserContractById(identity)

        return contract.methods.identityKey().call()
    }

    async getUserEthereumAddress(channelAddress) {
        const contract = await this.getUserContractById(channelAddress)
        
        return contract.methods.owner().call()
    }

    async userExists(account = this.account.address) {
        return 0 !== parseInt(await this.userManager.methods.users(account).call(), 16)
    }

    async signUp(identityKey) {
        return this.sendTransaction(this.userManager, 'signUp', [ Web3.utils.toHex(identityKey) ])
    }

    async getOpkStoreAddress(account = this.account.address) {
        const contract = await this.getUserContract(account)

        return Web3.utils.hexToUtf8(
            await contract.methods.opkStoreAddress().call()
        )
    }

    async getOpkStoreOffset(account = this.account.address) {
        const contract = await this.getUserContract(account)

        return contract.methods.opkStoreOffset().call()
    }

    async getOpkStoreSize(account = this.account.address) {
        const contract = await this.getUserContract(account)

        return contract.methods.opkStoreSize().call()
    }

    async refreshSpk(signedPreKey, signature) {
        return this.sendTransaction(
            await this.getUserContract(),
            'refreshSpk',
            [ signedPreKey, signature ].map(Web3.utils.toHex)
        )
    }

    async refreshOpkStore(storeLocator, storeSize) {
        return this.sendTransaction(
            await this.getUserContract(),
            'refreshOpkStore',
            [ Web3.utils.utf8ToHex(storeLocator), storeSize ]
        )
    }

    sign(message) {
        const messageHash = hashString(message)
        return this.account.sign(messageHash).signature
    }

    async claimIdentity(token, address) {
        const tokenHash = hash(token)
        const eventFilter = { fromBlock : 0, filter: { tokenHash: tokenHash } }

        return new Promise((resolve, reject) => {
            const rejectIfSet = (error) => { if (error) reject(error) }
            let txHash

            this.identityManager.events.IdentityApproved(eventFilter, async error => {
                rejectIfSet(error)
                txHash = this.sendTransaction(this.identityManager, 'claimIdentity', [ token, hashString(address) ])
            })

            this.identityManager.events.IdentityClaimed(eventFilter, (error, _) => {
                rejectIfSet(error)
                resolve(txHash)
            })
        })
    }

    async getIdentity(address) {
        const addressHash = hashString(address)

        return this.identityManager.methods.getIdentity(addressHash).call()
    }

    async getAuthority(ethAddress) {
        return this.identityManager.methods.authorities(ethAddress).call()
    }

    async rejectIdentity(token) {
        return this.sendTransaction(this.identityManager, 'rejectIdentity', [ token ])
    }

    async deleteIdentity(channelAddress) {
        return this.sendTransaction(this.identityManager, 'deleteIdentity', [ hashString(channelAddress) ])
    }

    async claimKeyBundle(identity) {
        const userContract = await this.getUserContract()
        const targetContract = await this.getUserContractById(identity)
        const targetAddress = await targetContract.methods.owner().call()
        const eventFilter = { filter: { owner: targetAddress } }
        const eventPromise = new Promise((resolve, reject) => {
            userContract.once('keyBundleClaimed', eventFilter, async (error, event) => {
                if (error) {
                    reject(error)
                }

                resolve({
                    identityKey    : Buffer.from((await targetContract.methods.identityKey().call()).slice(2), 'hex'),
                    registrationId : targetAddress, // what actually is this?
                    preKey: {
                        keyId      : event.returnValues.opkStoreOffset,
                        cid        : Web3.utils.hexToUtf8(event.returnValues.opkStoreAddress),
                        publicKey  : undefined // caller needs to do the query
                    },
                    signedPreKey: {
                        keyId      : 0,
                        publicKey  : Buffer.from(event.returnValues.signedPrekey.slice(2), 'hex'),
                        signature  : Buffer.from(event.returnValues.idSigned.slice(2), 'hex')
                    }
                })
            })
        })

        const txHash = this.sendTransaction(userContract, 'claimKeyBundle', [ targetContract.options.address ])

        return Promise.all([ txHash, eventPromise ])
    }

    async updateBallot() {
        return this.sendTransaction(this.senate, 'update', [])
    }

    async vote(id, voteFor) {
        return this.sendTransaction(this.senate, 'vote', [ id, voteFor ])
    }

    async getBallot() {
        return this.senate.methods.ballot().call()
    }

    async createBallot(topic, subject, description) {
        return this.sendTransaction(this.senate, 'createBallot', [ topic, subject, description ])
    }
}

export default EthConnector 
