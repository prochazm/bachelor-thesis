'use strict'


import http from 'https'
import Ipfs from 'ipfs-http-client'

class IpfsConnector {

    #connection
    #node
    #cache = []

    constructor(config) {
        if (config?.node == null) {
            throw new Error('IPFS node undefined')
        }

        if (config.node?.protocol == "http") {
            console.log("Warn: using http protocol")
        }

        this.#connection = Ipfs(config.node)
        this.#node = config.node
    }

    store(data) {
        return new Promise(async resolve => {
            const result = await this.#connection.add(data)
            resolve(result.path)
        })
    }

    read(hash, offset) {
        return new Promise((resolve, reject) => {
            if (this.#cache[hash]) {
                resolve(this.#cache[hash][offset])
                return
            }

            http.get(`${this.#node.protocol}://${this.#node.host}/ipfs/${hash}`, response => {
                let buffer = []

                response.on('data', d => buffer.push(d.toString()))
                response.on('end', () => {
                    this.#cache[hash] = buffer.join('').split('\n');
                    resolve(this.#cache[hash][offset])
                })
                response.on('error', reject)
            })
        })
    }
}

export default IpfsConnector
