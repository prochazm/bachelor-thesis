'use strict'

import utils from 'web3-utils'


class VotingManager {

    #ethConnector

    constructor(ethConnector) {
        this.#ethConnector = ethConnector
    }

    async #initChecked(type, subject, description = [ '' ]) {
        if (!utils.isAddress(subject)) {
            throw 'Subject should be an Ethereum address'
        }

        const txHash = await this.#ethConnector.createBallot(type, subject, description.join(' '))
        const ballot = await this.#ethConnector.getBallot()

        return [ txHash, ballot.id ]
    }

    async get() {
        const ballot = await this.#ethConnector.getBallot()

        Object.keys(ballot)
            .filter(isFinite)
            .forEach(k => delete ballot[k])

        ballot['description'] = utils.hexToUtf8(ballot['description'] || '0x')
        ballot['deadline'] = new Date(parseInt(ballot['deadline']) * 1000)
        ballot['state'] = [ 'Resolved', 'Pending' ][ballot['state']]
        ballot['topic'] = [
            'Senator invitation',
            'Senator uninvite',
            'Authority invitation',
            'Authority revocation'
        ][ballot['topic']]

        return ballot
    }

    async initSenatorRevocation(candidate, ...description) {
        return this.#initChecked(1, candidate, description)
    }

    async initSenatorInvitation(candidate, ...description) {
        return this.#initChecked(0, candidate, description)
    }

    async initAuthorityInvitation(authority, ...description) {
        return this.#initChecked(2, authority, description)
    }

    async initAuthorityRevocation(authority, ...description) {
        return this.#initChecked(3, authority, description)
    }

    async vote(id, verdict) {
        if (!['accept', 'reject'].includes(verdict)) {
            throw 'Verdict should be either [accept] or [reject]'
        }

        return this.#ethConnector.vote(id, verdict)
    }
}

export default VotingManager
