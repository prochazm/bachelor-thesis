'use strict'

import readline from 'readline'
import fs from 'fs'
import InterfaceFactory from './InterfaceFactory.js'


const config = ((path) => {
    try {
        return JSON.parse(fs.readFileSync(path))
    } catch {
        console.error('Could not parse configuration file')
        process.exit(1)
    }
})(process.argv[2])

const [CLI, helpLines] = (() => {
    try {
        return new InterfaceFactory(config).createInterface()
    } catch (error) {
        console.log(error)
        process.exit(2)
    }
})()

const descend = (line) => {
    let tokens = line.trim().split(/[\s]+/)
    let call = CLI
    let next = 0

    tokens.every(t => call[t] && (++next, call = call[t]))

    return [ tokens, call, next ]
}

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    completer: (line) => {
        const [tokens, call, next] = descend(line)
        const completions = Object.keys(call).map((k) => k + ' ')
        const hits = completions
            .filter(c => c.startsWith(tokens[next]))
            .map(c => (next ? `${tokens.slice(0, next).join(' ')} `: '') + c);

        return [hits.length ? hits : completions, tokens.join(' ')]
    }
})

const printResult = (results) => results && [].concat(results).forEach(i => console.log(i))

rl.setPrompt('> ')
rl.prompt()
rl.on('line', async (line) => {
    const [tokens, call, next] = descend(line)

    try {
        const results = await (async () => {
            if (tokens[next] == '?') {
                let node = helpLines
                tokens.slice(0, next).every(t => node = node?.[t])
                return node
            }

            if (typeof call !== 'function') {
                throw 'Command not found'
            }

            return call(tokens.slice(next))
        })()

        return printResult(results)
    } catch (error) {
        if (typeof error == 'string') {
            console.log(error)
        } else {
            console.error(`Line:\t${line}\nTokens:\t${tokens}`)
            console.error(error)
            console.log(error.message)
        }
    } finally {
        rl.prompt()
    }
})
