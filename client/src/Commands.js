'use strict'

import { inspect } from 'util'
import freeze from 'deep-freeze-es6'


export const commandMapping = (authenticationProtocol, signalProtocol, votingManager) => { 
    const bindToArray = (object, method) => (args) => object[method].apply(object, args)
    const mapping = freeze({
        'signal': ((o) => {
            const bind = bindToArray.bind(null, o)
            return {
                'register': bind('signUp'),
                'send'    : bind('send'),
                'receive' : bind('receive'),
                'status'  : bind('getKeyStatus'),
                'clear'   : bind('clearSessions'),
                'setdev'  : bind('changeDevice'),
                'update'  : {
                    'opk' : bind('updatePreKey'),
                    'spk' : bind('updateSignedPreKey'),
                }
            }
        })(signalProtocol),

        'identity': ((o) => {
            const bind = bindToArray.bind(null, o)
            return {
                'verify'  : bind('verify'),
                'claim'   : bind('claim'),
                'reject'  : bind('reject'),
                'disown'  : bind('disown'),
                'whois'   : bind('whois'),
            }
        })(authenticationProtocol),

        'senate': ((o) => {
            const bind = bindToArray.bind(null, o)
            return {
                'init': {
                    'invite': {
                        'authority' : bind('initAuthorityInvitation'),
                        'senator'   : bind('initSenatorInvitation'),
                    },
                    'revoke': {
                        'authority' : bind('initAuthorityRevocation'),
                        'senator'   : bind('initSenatorRevocation'),
                    }
                },
                'vote'  : bind('vote'),
                'print' : bind('get'),
            } 
        })(votingManager),

        'quit': () => process.exit(0),
        'help': {
            'tree': () => inspect(mapping, {
                showHidden: false,
                depth: null,
                colors: true,
                compact: false
            }),
            'info': () => "This interface supports very simplistic tab completion.\n" + 
                "To print command structure type 'help tree', to get help with\n" +
                "concrete command, type '?' as its argument.\n\n"+
                "How to invoke commands: Command consists of one or more words,\n" +
                "these are separated by space. After command is constructed arguments \n" +
                "follow, these are too separated by whitespace.\n\n" +
                "How to read '?' lines: '?' lines consist of two paragraphs, the first \n" +
                "describes function of the command and the other demonstrates the usage \n" +
                "Arguments are either <mandatory> or [optional], if argument is \n" +
                "prepended by ...dots then it consists of multiple words."
        },
    })

    return mapping
}

export const commandHelp = freeze({
    'signal': {
        'register': "Registers user if not already registered.\n" +
            "Usage: signal register",
        'send'    : "Encrypt input file with current device and store the result in output file, recipient is specified by his address.\n" +
            "If output file is not specified results will only be printed to stdout.\n" +
            "Usage: signal send <recipient> <input file> [output file]",
        'receive' : "Decrypt input with current device file and store the plaintext in output file, sender is specified by his address." +
            "If output file is not specified results will only be printed to stdout.\n" +
            "Usage: signal receive <sender> <input> [output]",
        'status'  : "Prints status of one-time keys including IPFS cid, number of used and remaining keys.\n" +
            "Usage: signal status",
        'clear'   : "Closes all sessions established with current device.\nUsage: signal clear",
        'setdev'  : "Sets current device (aka identity).\nUsage: signal setdev <identity>",
        'update'  : {
            'opk' : "Refresh one-time prekeys, user can set how many keys are generated.\nUsage: signal update opk [count]",
            'spk' : "Refresh signed prekey.\nUsage: signal update spk",
        }
    },
    'identity': {
        'verify'  : "Send verification request to authority specified by url.\nUsage: identity verify <url> <identity> <channel>",
        'claim'   : "Claim verified identity using token received from authority.\nUsage: identity claim <identity> <token>",
        'reject'  : "Reject verified identity using token received from authority.\nUsage: identity reject <identity> <token>",
        'disown'  : "Delete existing identity.\nUsage: identity delete <identity>",
        'whois'   : "Print identity details such as contract and owner address and validation timestamps.\n"+
            "Usage: identity whois <identity>",
    },
    'senate': {
        'init': {
            'invite': {
                'authority' : "Propose invitation of verification authority specified by its Ethereum address\n" +
                    "Usage: invite authority <address> [...description]",
                'senator'   : "Propose invitation of a senator specified by their Ethereum address\n" +
                    "Usage: invite senator <address> [...description]",
            },
            'revoke': {
                'authority' : "Propose revocation of verification authority specified by its Ethereum address\n" +
                    "Usage: revoke authority <address> [...description]",
                'senator'   : "Propose rejection of senator specified by their Ethereum address\n" +
                    "Usage: revoke senator <address> [...description]",
            }
        },
        'vote'  : "Vote for ballot defined by its id\nUsage: vote <id> <accept|reject>",
        'print' : "Prints active ballot\nUsage: print",
    },
    'quit': "Quit",
})
