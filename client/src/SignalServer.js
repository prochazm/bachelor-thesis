'use strict'


function serialize(keys) {
    return keys.map(k => k.toString('hex')).join('\n')
}

class SignalServer {

    #ethConnector
    #ipfsConnector
    
    constructor(ethConnector, ipfsConnector) {
        this.#ethConnector = ethConnector
        this.#ipfsConnector = ipfsConnector
    }

    async register(preKeyBundle) {
        if (await this.#ethConnector.userExists()) {
            throw 'User already exists'
        }

        const signUpTx = await this.#ethConnector.signUp(preKeyBundle.identityKey)

        return Promise.all([
            Promise.resolve(signUpTx), // this needs to be done first
            this.replaceSignedPreKey(preKeyBundle.signedPreKey),
            this.replaceOneTimePreKeys(preKeyBundle.preKey)
        ])
    }

    async replaceSignedPreKey(signedPreKey) {
        return this.#ethConnector.refreshSpk(
            signedPreKey.publicKey, 
            signedPreKey.signature
        )
    }

    async getIndentityKey(address) {
        return this.#ethConnector.getIndentityKey(address)
    }

    async getUserId(address) {
        return this.#ethConnector.getUserEthereumAddress(address)
    }

    async replaceOneTimePreKeys(preKeys) {
        const cid = await this.#ipfsConnector.store(serialize(preKeys))

        return this.#ethConnector.refreshOpkStore(cid, preKeys.length)
    }

    async getPreKeyBundle(userEth) {
        const [ txHash, keyBundle ] = await this.#ethConnector.claimKeyBundle(userEth)
        const opkString = await this.#ipfsConnector.read(keyBundle.preKey.cid, keyBundle.preKey.keyId)

        keyBundle.preKey.publicKey = Buffer.from(opkString, 'hex')

        return [ txHash, keyBundle ]
    }

    async getPreKeyStatus(userEth) {
        const opkStoreAddress = this.#ethConnector.getOpkStoreAddress(userEth)
        const opkStoreOffset  = this.#ethConnector.getOpkStoreOffset(userEth)
        const opkStoreSize    = this.#ethConnector.getOpkStoreSize(userEth)

        return Promise.all([ opkStoreAddress, opkStoreOffset, opkStoreSize ])
    }
}

export default SignalServer
