'use strict'

import EthConnector from './EthConnector.js'
import IpfsConnector from './IpfsConnector.js'
import SignalServer from './SignalServer.js'
import SignalStorage from './SignalStorage.js'
import SignalProtocol from './SignalProtocol.js'
import AuthenticationProtocol from './AuthenticationProtocol.js'
import VotingManager from './VotingManager.js'
import { commandMapping, commandHelp }from './Commands.js'


class InterfaceFactory {

    #signalProtocol
    #authenticationProtocol
    #votingManager

    constructor(config) {
        const ethConnector = new EthConnector(config?.ethereum)
        const ipfsConnector = new IpfsConnector(config?.ipfs)
        const signalServer = new SignalServer(ethConnector, ipfsConnector)
        const signalStorage = new SignalStorage(config?.signal?.storagePath)

        this.#signalProtocol = new SignalProtocol(signalServer, signalStorage, config?.signal?.defaultKeyBatchSize)
        this.#authenticationProtocol = new AuthenticationProtocol(ethConnector)
        this.#votingManager = new VotingManager(ethConnector)
    }

    createInterface() {
        return [commandMapping(
            this.#authenticationProtocol,
            this.#signalProtocol,
            this.#votingManager
        ), commandHelp]
    }
}

export default InterfaceFactory
