'use strict'

import fs from 'fs'
import signal from 'libsignal'


function isNullish(i) {
    return i == null
}

class SignalStorage {
    #store = {}
    #path
    #currentDevice

    constructor(path) {
        this.#path = path
        this.#deserialize()
        this.#currentDevice = undefined
    }

    #loadPreKeyHelper(keyType, keyId) {
        let res = this.get(keyType + keyId)
        if (res != null) {
            res = {
                pubKey: res.pubKey,
                privKey: res.privKey
            }
        }

        return Promise.resolve(res)
    }

    #serialize() {
        fs.writeFileSync(this.#path, JSON.stringify(this.#store))
    }

    #deserialize() {
        try {
            const blob = fs.readFileSync(this.#path)
            this.#store = JSON.parse(blob, (_, val) => val?.type === 'Buffer' ? Buffer.from(val.data) : val )
        } catch (error) {
            console.log(`Failed to read signal storage at ${this.#path}, creating a new one...`)
            try {
                fs.writeFileSync(this.#path, "")
            } catch (error) {
                console.error(error)
                exit(2) // can't recover, let it crash...
            }
        }
    }

    getOurIdentity() {
        return Promise.resolve(this.get('identityKey'))
    }

    getOurRegistrationId() {
        return Promise.resolve(this.get('registrationId'))
    }

    put(key, val) {
        if ([key, val].some(isNullish)) {
            throw new Error('Could not store nullish pair')
        }

        this.#store[key] = val

        this.#serialize()
    }

    get(key, defVal) {
        if (key == null) {
            throw new Error('Could not get value of nullish key')
        }

        return this.#store?.[key] ?? defVal
    }

    remove(key) {
        if (key == null) {
            throw new Error('Could not remove nullish key')
        }

        delete this.#store[key]
        this.#serialize()
    }

    isTrustedIdentity(id, idKey) {
        if (id == null) {
            throw new Error('Could not check identity')
        }

        let trusted = this.get('identityKey' + id)
        if (trusted == null) {
            return Promise.resolve(true)
        }

        return Promise.resolve(idKey == trusted)
    }

    loadIdentityKey(id) {
        if (id == null) {
            throw new Error('Could not get key for nullish identity')
        }

        return Promise.resolve(this.get('identityKey' + identifier))
    }

    saveIdentity(id, idKey) {
        if ([id, idKey].some(isNullish)) {
            throw new Error('Could not save nullish id or idKey')
        }

        return Promise.resolve(this.put('identityKey' + id, idKey))
    }

    loadPreKey(keyId) {
        return this.#loadPreKeyHelper('25519KeypreKey', keyId)
    }

    storePreKey(keyId, keyPair) {
        return Promise.resolve(this.put('25519KeypreKey' + keyId, keyPair))
    }

    removePreKey(keyId) {
        return Promise.resolve(this.remove('25519KeypreKey' + keyId))
    }

    loadSignedPreKey(keyId) {
        return this.#loadPreKeyHelper('25519KeysignedKey', keyId)
    }

    storeSignedPreKey(keyId, keyPair) {
        return Promise.resolve(this.put('25519KeysignedKey' + keyId, keyPair))
    }

    removeSignedPreKey(keyId) {
        return Promise.resolve(this.remove('25519KeysignedKey' + keyId))
    }

    loadSession(id) {
        if (!this.#currentDevice) {
            throw new Error('Current device is undefined')
        }

        const session = this.get(`session${this.#currentDevice}.${id}`)
        
        return Promise.resolve(session ? signal.SessionRecord.deserialize(session) : null)
    }

    storeSession(id, record) {
        if (!this.#currentDevice) {
            throw new Error('Current device is undefined')
        }

        const serializedRecord = record.serialize()
        
        return Promise.resolve(this.put(`session${this.#currentDevice}.${id}`, serializedRecord))
    }

    removeSession(id) {
        if (!this.#currentDevice) {
            throw new Error('Current device is undefined')
        }

        return Promise.resolve(this.remove(`session${this.#currentDevice}.${id}`))
    }

    removeAllSessions(id) {
        if (!this.#currentDevice) {
            throw new Error('Current device is undefined')
        }

        for (const key in this.#store) {
            if (key.startsWith(`session${this.#currentDevice}.${id}`)) {
                this.remove(key)
            }
        }
    }

    setDevice(id) {
        this.#currentDevice = id
    }
}

export default SignalStorage
