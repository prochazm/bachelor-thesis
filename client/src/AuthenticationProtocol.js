'use strict'

import { requestVerification } from './VAConnector.js'
import { URL } from 'url'


class AuthenticationProtocol {

    #ethConnector

    constructor(ethConnector) {
        this.#ethConnector = ethConnector
    }

    #validateString(arg, name, size) {
        if (!arg) {
            throw `${name} undefined`
        }

        if (typeof arg !== 'string') {
            throw `${name} has to be a string`
        }

        if (size && arg.length < size) {
            throw `${name} has to be 16 characters or longer`
        }
    }

    #validateUrl(url) {
        if (!url) throw 'Authority URL undefined'
        try {
            new URL(url)
        } catch {
            throw `Argument is not a valid url`
        }
    }

    async verify(authorityUrl, address, channel) {
        this.#validateUrl(authorityUrl)
        this.#validateString(channel, 'Channel')
        this.#validateString(address, 'Address')

        return requestVerification(authorityUrl, {
            address: address,
            channel: channel,
            signature: this.#ethConnector.sign(address),
        })
    }

    async claim(address, token) {
        this.#validateString(address, 'Address')
        this.#validateString(token, 'Token', 16)

        return this.#ethConnector.claimIdentity(token, address)
    }

    async reject(address, token) {
        this.#validateString(address, 'Address')
        this.#validateString(token, 'Token', 16)

        return this.#ethConnector.rejectIdentity(token, address)
    }

    async disown(address) {
        this.#validateString(address, 'Address')

        return this.#ethConnector.deleteIdentity(address)
    }

    async whois(address) {
        this.#validateString(address, 'Address')

        const userId = await this.#ethConnector.getUserEthereumAddress(address)
        const identity = Object.assign({}, await this.#ethConnector.getIdentity(address))

        Object.keys(identity).filter(isFinite).forEach(k => delete identity[k])

        identity['validationTimestamp'] = identity['validationTimestamp'].map(ts => {
            return new Date(parseInt(ts) * 1000)
        })

        identity.validated = [ ]
        for (let i = 0; i < identity['validators'].length; ++i) {
            identity['validated'].push(`${identity.validators[i]}: ${identity.validationTimestamp[i]}`)
        }

        delete identity['validators']
        delete identity['validationTimestamp']

        return {
            Ethereum: userId,
            Identity: identity
        }
    }
}

export default AuthenticationProtocol
